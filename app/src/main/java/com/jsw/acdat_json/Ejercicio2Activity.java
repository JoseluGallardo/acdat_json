package com.jsw.acdat_json;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class Ejercicio2Activity extends AppCompatActivity {

    @BindView(R.id.spinner)
    Spinner mSpnCity;
    private Context mContext;
    private AsyncHttpClient mClient;
    private ArrayList<CityData> cityList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);
        this.mContext = this;

        ButterKnife.bind(this);

        cityList = new ArrayList<>();
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item);
        adapter.addAll(getResources().getStringArray(R.array.city));
        mSpnCity.setAdapter(adapter);
    }

    public void downloadCity(View v) {
        mClient = new AsyncHttpClient();
        String url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" +
                mSpnCity.getSelectedItem().toString() +
                "+&mode=json&units=metric&cnt=7&appid=73a35173c707e2493f2bd5efe2529613";
        final String JSON_FILE = "cityData.json";
        final String XML_FILE = "cityData.xml";


        mClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(mContext, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    cityList = Utils.getCityInfo(response);
                    Utils.almacenarJSON(cityList, JSON_FILE);
                    Utils.almacenarXML(cityList, XML_FILE, mSpnCity.getSelectedItem().toString());
                    Toast.makeText(mContext, "Download Succesful", Toast.LENGTH_LONG).show();
                    finish();
                } catch (Exception e) {
                    Toast.makeText(Ejercicio2Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

class CityData {
    String date;
    double maxTemp;
    double minTemp;
    double preassure;
}
