package com.jsw.acdat_json;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class Ejercicio1Activity extends AppCompatActivity {

    @BindView(R.id.spn_city)
    Spinner mSpnCity;
    @BindView(R.id.tv_city)
    TextView mTvCity;
    @BindView(R.id.iv_icono)
    ImageView mIvIcon;
    @BindView(R.id.tv_grados)
    TextView mTvGrados;
    @BindView(R.id.tv_tiempo)
    TextView mTvTiempo;
    @BindView(R.id.tv_cloud)
    TextView mTvCloud;
    @BindView(R.id.tv_preassure)
    TextView mTvPreassure;
    @BindView(R.id.tv_humidity)
    TextView mTvHumidity;
    @BindView(R.id.tv_sunrise)
    TextView mTvSunrise;
    @BindView(R.id.tv_sunset)
    TextView mTvSunset;
    AsyncHttpClient client;
    Data cityData;
    Context context;
    String URL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        ButterKnife.bind(this);

        mSpnCity.getAdapter();
        mSpnCity.setAdapter(new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, getResources().getTextArray(R.array.city)));
        this.context = this;
        mostrar();
    }

    private void mostrar() {
        URL = "http://api.openweathermap.org/data/2.5/weather?q=" + mSpnCity.getSelectedItem().toString() + ",es&appid=73a35173c707e2493f2bd5efe2529613";
        client = new AsyncHttpClient();

        client.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    cityData = Utils.getWeatherInfo(response);
                    Toast.makeText(context, "Descarga completada", Toast.LENGTH_SHORT).show();
                    updateData(cityData);
                } catch (JSONException e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void updateData(Data data) {

        Double temp = Double.parseDouble(data.Grados);
        temp = temp / 32;

        mTvCity.setText(data.City);
        Picasso.with(context).load("http://openweathermap.org/img/w/" + data.Icon + ".png").resize(300, 300).into(this.mIvIcon);
        mTvGrados.setText("Temp: " + String.valueOf(temp).substring(0, 3) + " ºC");
        mTvTiempo.setText(data.Tiempo);
        mTvCloud.setText("Cloudiness: " + data.Cloud);
        mTvPreassure.setText("Preassure: " + data.Preassure);
        mTvHumidity.setText("Humidity: " + data.Humidity);
        mTvSunrise.setText("Sunrise: " + data.Sunrise);
        mTvSunset.setText("Sunset: " + data.Sunset);
    }

    public void cambiarCiudad(View v) {
        mostrar();
    }
}
