# README #

## Ejercicios JSON ##
### José Luis del Pino Gallardo ###


### Ejercicio 4 ###

* Muestra los últimos resultados de la jornada de la liga nacional de fútbol americano.
![Screenshot_1483872091.png](https://bitbucket.org/repo/7jXgXk/images/1087723432-Screenshot_1483872091.png)

Los datos han sido recogidos en formato XML de la página oficial de la NFL:
[http://www.nfl.com/liveupdate/scorestrip/ss.xml](Link URL)